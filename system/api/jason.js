// Jason reads and writes JSON files. Please be nice to Jason.
const fs = require('fs');
module.exports = {
    readFile: function(file) {
        fs.readFile(file, (err, data) => {
            if (err) throw err;
            let output = JSON.parse(data);
            return output;
        });
    }
}