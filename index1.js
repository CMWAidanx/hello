var d = new Date
if (d.getHours() < 12) {
    document.getElementById("hello").innerHTML = "<h1>Good Morning.</h1>"
} else if (d.getHours() == 12) {
    document.getElementById("hello").innerHTML = "<h1>You're halfway there.</h1>"
} else if (d.getHours() > 12 && d.getHours() < 6) {
    document.getElementById("hello").innerHTML = "<h1>Good Afternoon.</h1>"
} else {
    document.getElementById("hello").innerHTML = "<h1>Good Evening.</h1>"
}
